<?php
header('Access-Control-Allow-Origin: *');
if (!isset($_GET["claimId"]) || $_GET["claimId"] == "")
    exit;
$debug = (isset($_GET["debug"]));
$test  = false;
if (isset($_GET["env"])) {
    $test = (($_GET["env"]) == "TEST");
}
$fixLabourHours = false;
if (isset($_GET["fixLaborHours"])) {
    $fixLabourHours = true;
}

if (isset($_GET["debug"]))
    $debug = true;
//The AXA credentials to use the web service 
if (!$test) {
    $site                      = "noncdn-www.datgroup.com";
    $site2                     = "www.dat.eu";
    $customerLogin             = "axainsurance";
    $customerNumber            = "3132952";
    $customerSignature         = "akEwRUF3TUNxTUt5NlFmTTVCdGd5UzYwbEdxcW1qdHpQRGFoSlJneHBvTGhreURMc0VCOG0raHJJd3F3ZVpnWFpMN1dCMTZRUVdSMlRqU2lDemVS";
    $interfacePartnerNumber    = "3132952";
    $interfacePartnerSignature = "AA1B25142947B830DD57B58409E479409DA18EADD5866E0DC0AB9D0F8BD0419B";
} else {
    $site                      = "noncdn-gold.datgroup.com";
    $site2                     = "www.dat.eu";
    $customerLogin             = "axainsurance";
    $customerNumber            = "3132952";
    $customerSignature         = "akEwRUF3TUM5OFdteG1PMFRWWmd5UzV2alplUlB3dnJwcXR0UG1EVW85ZlAzay9jekZad1NQck8zZVhKOU80ekJVOW96RmdZQzNIUUoyYmpFck1v";
    $interfacePartnerNumber    = "3132952";
    $interfacePartnerSignature = "AA1B25142947B830DD57B58409E479409DA18EADD5866E0DC0AB9D0F8BD0419B";
}

//Create the Web Service client with the credentials
$opts = array(
    'http' => array(
        'header' => "customerLogin: " . $customerLogin . "\r\n" . "customerNumber: " . $customerNumber . "\r\n" . "customerSignature: " . $customerSignature . "\r\n" . "interfacePartnerNumber: " . $interfacePartnerNumber . "\r\n" . "interfacePartnerSignature: " . $interfacePartnerSignature . "\r\n"
    )
);
sleep(4);
$opts   = stream_context_create($opts);
$client = new SoapClient("http://" . $site . "/myClaim/soap/v2/MyClaimExternalService?wsdl", array(
    'stream_context' => $opts,
    'exceptions' => true,
    'trace' => 1,
    'cache_wsdl' => 'WSDL_CACHE_NONE'
));

//Now we need to get all the attachments
$request = "<myc:listAttachmentsOfContract xmlns:myc='http://" . $site2 . "/myClaim/soap/v2/MyClaimExternalService'>
    <contractId>" . $_GET["claimId"] . "</contractId>
    <documentTypes>99</documentTypes>
</myc:listAttachmentsOfContract>";
$request = new SoapVar($request, XSD_ANYXML);
try {
    $listAttachments = $client->__soapCall("listAttachmentsOfContract", array(
        $request
    ));
}
catch (SoapFault $e) {
    echo "ERROR : " . $e->faultstring;
    exit;
}
//If there are no attachments, we can't rewrite the revision info, so we stop the execution
if (!isset($listAttachments->return)) {
    echo "No attachments found";
    exit;
}


//Now we change the status to "N.A. Processing", which deletes all the attachments, creating "blank.txt"

$request = "<myc:changeContractStatus xmlns:myc='http://" . $site2 . "/myClaim/soap/v2/MyClaimExternalService'>
    <contractId>" . $_GET["claimId"] . "</contractId>
    <statusType>N.A. Processing</statusType>
</myc:changeContractStatus>";
$request = new SoapVar($request, XSD_ANYXML);
try {
    $upload = $client->__soapCall("changeContractStatus", array(
        $request
    ));
}
catch (SoapFault $e) {
    echo "ERROR : " . $e->faultstring;
    exit;
}

//First we get the claim data to take the info from "revision4" field
$request = "<myc:getContract xmlns:myc='http://" . $site2 . "/myClaim/soap/v2/MyClaimExternalService'>
    <contractId>" . $_GET["claimId"] . "</contractId>
    <isWithHistoricalCalculations></isWithHistoricalCalculations>
</myc:getContract>";
$request = new SoapVar($request, XSD_ANYXML);
try {
    $contract = $client->__soapCall("getContract", array(
        $request
    ));
}
catch (SoapFault $e) {
    echo "ERROR : " . $e->faultstring;
    exit;
}
$revision       = "";
$CoeffRiduzione = "";
if (isset($contract->return->customTemplateData->entry)) {
    if (count($contract->return->customTemplateData->entry) > 1)
        $entries = $contract->return->customTemplateData->entry;
    else
        $entries[] = $contract->return->customTemplateData->entry;
    foreach ($entries as $field) {
        if ($field->key == "revision4") {
            $revision = $field->value;
        }
        if ($field->key == "CoeffRiduzione") {
            $CoeffRiduzione = $field->value;
        }
    }
}
$positions           = $contract->return->Dossier->RepairCalculation->CalcResultItaly->PositionsItaly->PositionItaly;
$discountedPositions = array();

foreach ($positions as $position) {
    if ($position->PartDiscountValue != null) {
        $key                             = trim($position->PartNumber->_);
        $value                           = floatval($position->ValueParts->_);
        $discount                        = floatval($position->PartDiscountValue->_);
        $discountPerc                    = intval($position->PartDiscountPerc->_);
        $discountedVal                   = number_format(($value - $discount), 2);
        $discountedPositions[$key]['dv'] = str_pad($discountedVal, 8, '0', STR_PAD_LEFT);
        $discountedPositions[$key]['dp'] = 'S' . str_pad($discountPerc, 2, '0', STR_PAD_LEFT);
    }
}

if ($debug) {
    print_r($discountedPositions);
    exit;
}

//If the field is empty or it's not a date, we won't modify perizie.txt
if ($revision == "" || stripos($revision, "/") === FALSE) {
    $revision = "";
} else {
    //We "transform" the revision field, from DD/MM/YYYY to MMYY format
    $revision = explode("/", $revision);
    $revision = $revision[1] . substr($revision[2], 2, 2);
    echo "Revision : " . $revision . "<br>";
}

if (count($listAttachments->return) > 1)
    $listAttachmentsArray = $listAttachments->return;
else
    $listAttachmentsArray[] = $listAttachments->return;
foreach ($listAttachmentsArray as $attachment) {
    $client = new SoapClient("http://" . $site . "/myClaim/soap/v2/MyClaimExternalService?wsdl", array(
        'stream_context' => $opts,
        'exceptions' => true,
        'trace' => 1,
        'cache_wsdl' => 'WSDL_CACHE_NONE'
    ));
    //We ignore the blank.txt document, as it is empty allways
    if ($attachment->fileName == "blank.txt")
        continue;
    echo "File " . $attachment->fileName . " ...<br>";
    if ($attachment->fileName == "danni.txt") {
        $newfile_content = "";

        file_put_contents($attachment->fileName, $attachment->binaryData);
        $file = fopen($attachment->fileName, "r");

        while ($line = fgets($file)) {
            $sparePart = substr($line, 89, 19);
            $price     = (double) substr($line, 224, 8);

            $key = trim($sparePart);
            if (array_key_exists($key, $discountedPositions)) {
                $price = $discountedPositions[$key]['dv'];
                $line  = substr($line, 0, 224) . $price . substr($line, 232);
                //$discount = $discountedPositions[$key]['dp'];
                //$line = substr($line, 0, 239) . $discount . substr($line, 241);    
            }

            echo "Spare Parts : " . $sparePart . "<br>";
            echo "Price : " . $price . "<br>";
            echo "Character at column 233 : " . substr($line, 232, 1) . "<br>";
            if (strtoupper(substr($line, 232, 1)) == "C" || substr($line, 232, 1) == "#" || substr($line, 232, 1) == " ") {
                echo "wrong character detected<br>";
                if ($price == 0.00) {
                    $new_char = " ";
                } else {
                    if (strpos(substr($line, 89, 19), '9999') !== 0 and strpos(substr($line, 89, 19), '#') !== 0 and strpos(substr($line, 89, 19), ' ') !== 0) {
                        echo "not found first 9999 or #<br>";
                        $varEcho  = strpos(substr($line, 89, 19), '9999');
                        $varEcho2 = strpos(substr($line, 89, 19), '#');
                        echo $varEcho . "CHE STAMPA9999<br>";
                        echo $varEcho2 . "CHE STAMPA#<br>";
                        $new_char = "S";
                    } else {
                        $new_char = " ";
                    }
                }
                $line = substr($line, 0, 232) . $new_char . substr($line, 233);
            }
            if (strtoupper(substr($line, 232, 1)) == "S") {
                if ($price == 0.00) {
                    $new_char = " ";
                    $line     = substr($line, 0, 232) . $new_char . substr($line, 233);
                }

            }
            echo "Character at column 234 : " . substr($line, 233, 1) . "<br>";
            if (strtoupper(substr($line, 233, 1)) == "C" || substr($line, 233, 1) == "#" || substr($line, 233, 1) == " ") {
                echo "wrong character detected<br>";
                if ($price == 0.00) {
                    $new_char = " ";
                } else {
                    if (strpos(substr($line, 89, 19), '9999') !== 0 and strpos(substr($line, 89, 19), '#') !== 0 and strpos(substr($line, 89, 19), ' ') !== 0) {
                        echo "not found second 9999 or #<br>";
                        $new_char = "S";
                        if (strpos(substr($line, 89, 19), '9999') !== 0 and ($CoeffRiduzione !== 'No')) {
                            $line = substr($line, 0, 238) . "*" . substr($line, 239);
                        }
                    } else {
                        $new_char = " ";
                    }
                }

                $line = substr($line, 0, 233) . $new_char . substr($line, 234);
            }
            if (strtoupper(substr($line, 233, 1)) == "S") {
                if ($price == 0.00) {
                    $new_char = " ";
                    $line     = substr($line, 0, 233) . $new_char . substr($line, 234);
                }

            }
            echo "Character at column 235 : " . substr($line, 234, 1) . "<br>";
            if (strtoupper(substr($line, 234, 1)) == "C" || substr($line, 234, 1) == "#" || substr($line, 234, 1) == " ") {
                echo "wrong character detected<br>";
                if ($price == 0.00) {
                    $new_char = " ";
                } else {
                    if (strpos(substr($line, 89, 19), '9999') !== 0 and strpos(substr($line, 89, 19), '#') !== 0 and strpos(substr($line, 89, 19), ' ') !== 0) {
                        echo "not found third 9999 or #<br>";
                        $new_char = "S";
                    } else {
                        $new_char = " ";
                    }
                }
                $line = substr($line, 0, 234) . $new_char . substr($line, 235);
            }
            if (strtoupper(substr($line, 234, 1)) == "S") {
                if ($price == 0.00) {
                    $new_char = " ";
                    $line     = substr($line, 0, 234) . $new_char . substr($line, 235);
                }

            }
            $newfile_content .= $line;
        }

        fclose($file);
        unlink($attachment->fileName);
        //documentID=112560
        $binaryData = base64_encode($newfile_content);
        $request    = "<myc:uploadAttachmentByFolderID xmlns:myc='http://" . $site2 . "/myClaim/soap/v2/MyClaimExternalService'>
            <contractId>" . $_GET["claimId"] . "</contractId>
            <attachmentItem>
                <fileName>" . $attachment->fileName . "</fileName>
                <mimeType>" . $attachment->mimeType . "</mimeType>
                <binaryData>" . $binaryData . "</binaryData>
                 <documentID>" . $attachment->documentID . "</documentID>
             </attachmentItem>
        </myc:uploadAttachmentByFolderID>";
        $request    = new SoapVar($request, XSD_ANYXML);
        try {
            $upload = $client->__soapCall("uploadAttachmentByFolderID", array(
                $request
            ));
        }
        catch (SoapFault $e) {
            echo "ERROR : " . $e->faultstring;
            exit;
        }
    }
    else if ($attachment->fileName == "perizie.txt" && $revision != "") {
        $newfile_content = "";
        file_put_contents($attachment->fileName, $attachment->binaryData);
        $file = fopen($attachment->fileName, "r");
        //We read the content line by line (in fact there are only two lines, one empty)
        while ($line = fgets($file)) {
            if ($line != "") {
                //With the no-empty line, we will put the revision in format MMYY in the column 1070, in case that $revision is not empty. The rest of the file remains untouched
                echo "Revision OK<br>";
                $line = substr($line, 0, 1069) . $revision . substr($line, 1073);

                //fixnig TOTALE ORE MECCANICA
                if ($fixLabourHours) {
                    $oldLH = substr($line, 1928, 5);

                    if (strpos($oldLH, '.') === FALSE) {
                        $newLH = substr($oldLH, 1);     //cut the leading zero: 01050 -> 1050
                        $newLH = substr_replace($newLH, '.', 2, 0); //put '.' in the middle -> 10.50
                        $line = substr($line, 0, 1028) . $newLH . substr($line, 1033);
                    }
                }
            }
            $newfile_content .= $line;
        }
        fclose($file);
        unlink($attachment->fileName);
        //documentID=112560
        //And then we convert it to base64, so we can re-upload again to the claim
        $binaryData = base64_encode($newfile_content);
        $request    = "<myc:uploadAttachmentByFolderID xmlns:myc='http://" . $site2 . "/myClaim/soap/v2/MyClaimExternalService'>
            <contractId>" . $_GET["claimId"] . "</contractId>
            <attachmentItem>
                <fileName>" . $attachment->fileName . "</fileName>
                <mimeType>" . $attachment->mimeType . "</mimeType>
                <binaryData>" . $binaryData . "</binaryData>
                 <documentID>" . $attachment->documentID . "</documentID>
             </attachmentItem>
        </myc:uploadAttachmentByFolderID>";
        $request    = new SoapVar($request, XSD_ANYXML);
        try {
            $upload = $client->__soapCall("uploadAttachmentByFolderID", array(
                $request
            ));
        }
        catch (SoapFault $e) {
            echo "ERROR : " . $e->faultstring;
            exit;
        }
    }
    else {
        //We just upload the document again
        $request = "<myc:uploadAttachmentByFolderID xmlns:myc='http://" . $site2 . "/myClaim/soap/v2/MyClaimExternalService'>
            <contractId>" . $_GET["claimId"] . "</contractId>
            <attachmentItem>
                <fileName>" . $attachment->fileName . "</fileName>
                <mimeType>" . $attachment->mimeType . "</mimeType>
                <binaryData>" . base64_encode($attachment->binaryData) . "</binaryData>
                 <documentID>" . $attachment->documentID . "</documentID>
             </attachmentItem>
        </myc:uploadAttachmentByFolderID>";
        //echo $request."<br>";
        $request = new SoapVar($request, XSD_ANYXML);
        try {
            $upload = $client->__soapCall("uploadAttachmentByFolderID", array(
                $request
            ));
        }
        catch (SoapFault $e) {
            echo "ERROR Upload : " . $e->faultstring;
            exit;
        }
    }
}
echo "File uploaded";

$request = "<myc:changeContractStatus xmlns:myc='http://" . $site2 . "/myClaim/soap/v2/MyClaimExternalService'>
    <contractId>" . $_GET["claimId"] . "</contractId>
    <statusType>N.A. Perizia conclusa</statusType>
</myc:changeContractStatus>";
$request = new SoapVar($request, XSD_ANYXML);
try {
    $upload = $client->__soapCall("changeContractStatus", array(
        $request
    ));
}
catch (SoapFault $e) {
    echo "ERROR : " . $e->faultstring;
    exit;
}
echo "Status changed";
?>